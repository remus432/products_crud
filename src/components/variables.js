const variables = {
  primary: "#4b6584",
  btn_primary: "#4b7bec",
  btn_danger: "#fc5c65",
  btn_primary_hover: "#3867d6",
  btn_danger_hover: "#eb3b5a",
  bg: "#3867d6",
  err: "#eb3b5a"
}

export default variables